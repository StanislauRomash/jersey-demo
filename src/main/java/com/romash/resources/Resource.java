package com.romash.resources;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class Resource {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello(@DefaultValue("Guest") @QueryParam("name") String name) {
        return "Hello, " + name + "!";
    }
}
