package com.romash.resource;

import com.romash.resources.Resource;
import javax.ws.rs.core.Application;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTestNg;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ResourceTest extends JerseyTestNg.ContainerPerMethodTest {

    @Override
    protected Application configure() {
        return new ResourceConfig(Resource.class);
    }

    @Test
    public void testDefaultName() {
        final String hello = target("hello").request().get(String.class);
        assertEquals("Hello, Guest!", hello);
    }

    @Test
    public void testCustomName() {
        final String hello = target("hello")
                .queryParam("name", "Stas")
                .request().get(String.class);
        assertEquals("Hello, Stas!", hello);
    }
}
